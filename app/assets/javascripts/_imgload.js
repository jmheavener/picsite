;(function($, window, document, undefined) {
	var pluginName = 'imgload';
	var defaults = {
		threshold: 600
	};

	function Plugin(elements, options) {
		this.elements = elements;
		this.$window = $(window);
		this.settings = $.extend({}, defaults, options);
		this._defaults = defaults;
		this._name = pluginName;
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var self = this;
			this.elements = this.elements = $.grep(this.elements, function(element) {
				var $element = $(element);
				if ( $element.data('src') === undefined || $element.data(self._name+'_loaded') !== undefined ) {
					return false;
				}
				$element.data(self._name+'_loaded', false);
				return true;
			});
			
            this.$window.on('scroll resize', function() {
                self.check();
            });
            this.check();
		},
		check: function() {
			if ( this.elements.length ) {
				var self = this;
				var boundry = ( window.innerHeight ? window.innerHeight : this.$window.height() ) + this.$window.scrollTop() + this.settings.threshold;
				var elements = this.elements = $.grep(this.elements, function(element) {
					var $element = $(element);
					if ( !$element.data(self._name+'_loaded') && $element.offset().top <= boundry ) {
						if ( self.appear($element) ) {
							return false;
						}
					}
					return true;
				});
				this.elements = elements;
			}
		},
		appear: function($element) {
			$element.data(this._name+'_loaded', true);
			$('<img />').on('load', function() {
				$element.attr('src', $element.data('src')).addClass('in');
			}).attr('src', $element.data('src'));
			return true;
		}
	};

	$.fn[pluginName] = function(options) {
		if ( typeof options === undefined ) options = {};
		new Plugin(this, options);
		return this;
	};
})(jQuery, window, document);
